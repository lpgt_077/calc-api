import * as bodyParser from "body-parser";
import dotenv from "dotenv";
import express, { Express, Request, Response } from "express";
import RouterStatistics from "./statistics/router.statistics";

dotenv.config();

const app: Express = express();
const port = process.env.PORT;
app.use(bodyParser.json(), RouterStatistics);

app.get("/", (req: Request, res: Response) => {
  res.send("Hi there, looking for something?");
});

app.listen(port, () => {});
