import express from "express";
import * as ServiceStatistics from "./service.statistics";

const isArrayOfNumbers = (maybeArray: unknown): boolean =>
  Array.isArray(maybeArray) && !maybeArray.some(Number.isNaN);

const getMean = (req: express.Request, res: express.Response) => {
  const numbers =
    req.body.Object.hasOwnProperty.call("numbers") && req.body.numbers;
  if (numbers && isArrayOfNumbers(numbers)) {
    res.json({ mean: ServiceStatistics.calculateMean(req.body.numbers) });
  } else {
    res.sendStatus(400);
  }
};

const getMedian = (req: express.Request, res: express.Response) => {
  const numbers =
  req.body.Object.hasOwnProperty.call("numbers") && req.body.numbers;
  if (numbers && isArrayOfNumbers(numbers) && numbers.length > 0) {
    res.json({ median: ServiceStatistics.calculateMedian(req.body.numbers) });
  } else {
    res.sendStatus(400);
  }
};

const getStandardDeviation = (req: express.Request, res: express.Response) => {
  const numbers =
  req.body.Object.hasOwnProperty.call("numbers") && req.body.numbers;
  if (numbers && isArrayOfNumbers(numbers) && numbers.length > 0) {
    res.json({
      standardDeviation: ServiceStatistics.calculateStandardDeviation(
        req.body.numbers
      ),
    });
  } else {
    res.sendStatus(400);
  }
};

export { getMean, getMedian, getStandardDeviation };
