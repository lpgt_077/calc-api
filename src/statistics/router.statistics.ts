import express, { Router } from "express";
import * as ControllerStatistics from "./controller.statistics";

const RouterStatistics = Router();

RouterStatistics.post("/mean", (req: express.Request, res: express.Response) =>
  ControllerStatistics.getMean(req, res)
);

RouterStatistics.post(
  "/median",
  (req: express.Request, res: express.Response) =>
    ControllerStatistics.getMedian(req, res)
);

RouterStatistics.post(
  "/standard_deviation",
  (req: express.Request, res: express.Response) =>
    ControllerStatistics.getStandardDeviation(req, res)
);

export default RouterStatistics;
